package com.kmk.jenkindemo.jenkindemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenkindemoApplication {
    // run app
	public static void main(String[] args) {
		SpringApplication.run(JenkindemoApplication.class, args);
	}

}
